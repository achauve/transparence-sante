#!/usr/bin/env python3
import sys

from src import docker_container
from src.clean import clean_all
from src.download import download_raw_data
from src.postgres import setup_monitoring
from src.postgres import analyze_all
from src.postgres import upload_all


def setup():
    docker_container.setup()
    docker_container.start()
    setup_monitoring.run()
    download_raw_data()


def reset():
    upload_all.reset()


def run():
    docker_container.start()
    clean_all.run()
    upload_all.run()
    analyze_all.run()


if __name__ == "__main__":
    action_mapping = {
        'setup': setup,
        'reset': reset,
        'run': run,
        'setup-docker': docker_container.setup,
        'setup-data': download_raw_data,
    }
    actions_string = ', '.join(action_mapping.keys())

    if len(sys.argv) != 2:
        raise ValueError("Usage 'main.py action', with action in list '{}'".format(actions_string))

    action = sys.argv[1]
    if action in action_mapping:
        action_mapping[action]()
    else:
        print("Allowed actions are '{}'".format(actions_string))