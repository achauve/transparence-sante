from config.env import env, SERVER
if env == SERVER:
    HOST = '51.68.44.24'
else:
    HOST = 'localhost'

# POSTGRES
POSTGRES_PORT = 5432
POSTGRES_USER = 'postgres'
POSTGRES_HOST = HOST


# METABASE
METABASE_PORT = 3000
METABASE_HOST = HOST