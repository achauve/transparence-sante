from config.env import env, TEST
CHUNK_SIZE = 100000

if env == TEST:
    ROWS_LIMIT = 100
else:
    ROWS_LIMIT = None
