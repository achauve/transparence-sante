from pycodestyle import StyleGuide, normalize_paths


def test_coding_style():
    """Test that we conform to PEP-8."""
    style = StyleGuide(config_file='setup.cfg', quiet=False)
    style.options.exclude = normalize_paths('venv')
    result = style.check_files('.')
    assert result.total_errors == 0, \
        "Found {} code style errors (and warnings).\n" \
        "Run 'pycodestyle --exclude=venv .' to see the details.".format(result.total_errors)
