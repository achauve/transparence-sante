import pytest

from src.clean.clean_all import all_cleaners
from src.clean.declaration import remuneration, avantage, convention


def test_that_all_declaration_have_the_same_columns_list():
    avantage_columns = set(avantage.cleaner._cleaned_csv_columns)
    remuneration_columns = set(remuneration.cleaner._cleaned_csv_columns)
    convention_columns = set(convention.cleaner._cleaned_csv_columns)
    assert avantage_columns == remuneration_columns
    assert avantage_columns == convention_columns


# Avoid metabase errors in case of case change  # https://github.com/metabase/metabase/issues/7923
@pytest.mark.parametrize("cleaner", all_cleaners())
def test_that_cleaned_columns_are_lower_case(cleaner):
    for column_name in cleaner._cleaned_csv_columns:
        assert column_name.islower()
