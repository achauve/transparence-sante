
SET work_mem = '150MB'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW IF NOT EXISTS "declaration_aggregate_montant_ttc_by_entreprise_émmetrice" AS (
    SELECT
        "entreprise_émmetrice",
        sum("montant_ttc") AS "total_montant_ttc"
    FROM "declaration"
    WHERE (
        ("entreprise_émmetrice" IS NOT NULL)
        AND ("montant_ttc" IS NOT NULL)
    )
    GROUP BY "entreprise_émmetrice"
    ORDER BY (sum("montant_ttc")) DESC
);
RESET work_mem;

CREATE INDEX IF NOT EXISTS "declaration_aggregate_montant_ttc_by_entreprise_émmetrice_prefix_index_on_entreprise_émmetrice"
ON public."declaration_aggregate_montant_ttc_by_entreprise_émmetrice" (lower("entreprise_émmetrice") text_pattern_ops, "entreprise_émmetrice");

CREATE INDEX IF NOT EXISTS "declaration_aggregate_montant_ttc_by_entreprise_émmetrice_index_on_entreprise_émmetrice"
ON public."declaration_aggregate_montant_ttc_by_entreprise_émmetrice" ("entreprise_émmetrice");

CREATE INDEX IF NOT EXISTS "declaration_aggregate_montant_ttc_by_entreprise_émmetrice_index_on_total_montant_ttc"
ON public."declaration_aggregate_montant_ttc_by_entreprise_émmetrice" ("total_montant_ttc");
