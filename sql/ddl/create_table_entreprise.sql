
CREATE TABLE IF NOT EXISTS "entreprise" (
    "id" SERIAL PRIMARY KEY,
    "identifiant_entreprise" TEXT,
    "code_pays" TEXT,
    "pays" TEXT,
    "secteur" TEXT,
    "entreprise_émmetrice" TEXT,
    "code_postal" TEXT,
    "ville" TEXT,
    "adresse" TEXT
);
