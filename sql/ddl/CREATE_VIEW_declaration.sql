
CREATE OR REPLACE VIEW declaration AS (
    SELECT type_declaration || '_' || id as declaration_id, "catégorie", "date", "detail", "entreprise_émmetrice", "identifiant", "identifiant_convention", "montant_masque", "montant_ttc", "nom_prénom", "profession", "specialité", "type_declaration", "type_identifiant" FROM declaration_remuneration
    UNION ALL
    SELECT type_declaration || '_' || id as declaration_id, "catégorie", "date", "detail", "entreprise_émmetrice", "identifiant", "identifiant_convention", "montant_masque", "montant_ttc", "nom_prénom", "profession", "specialité", "type_declaration", "type_identifiant"  FROM declaration_convention
    UNION ALL
    SELECT type_declaration || '_' || id as declaration_id, "catégorie", "date", "detail", "entreprise_émmetrice", "identifiant", "identifiant_convention", "montant_masque", "montant_ttc", "nom_prénom", "profession", "specialité", "type_declaration", "type_identifiant" FROM declaration_avantage
);
