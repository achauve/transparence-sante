
CREATE TABLE IF NOT EXISTS "declaration_remuneration" (
    "id" SERIAL PRIMARY KEY,
    "entreprise_émmetrice" TEXT,
    "catégorie" TEXT,
    "profession" TEXT,
    "specialité" TEXT,
    "type_identifiant" TEXT,
    "identifiant" TEXT,
    "type_declaration" TEXT,
    "date" DATE,
    "montant_ttc" INT,
    "identifiant_convention" TEXT,
    "nom_prénom" TEXT,
    "detail" TEXT,
    "montant_masque" TEXT
);
