
CREATE TABLE IF NOT EXISTS "declaration_convention" (
    "id" SERIAL PRIMARY KEY,
    "entreprise_émmetrice" TEXT,
    "identifiant_convention" TEXT,
    "catégorie" TEXT,
    "profession" TEXT,
    "specialité" TEXT,
    "type_identifiant" TEXT,
    "identifiant" TEXT,
    "type_declaration" TEXT,
    "date" DATE,
    "montant_ttc" INT,
    "nom_prénom" TEXT,
    "detail" TEXT,
    "montant_masque" TEXT
);
