
CREATE TABLE IF NOT EXISTS "declaration_avantage" (
    "id" SERIAL PRIMARY KEY,
    "entreprise_émmetrice" TEXT,
    "catégorie" TEXT,
    "profession" TEXT,
    "specialité" TEXT,
    "type_identifiant" TEXT,
    "identifiant" TEXT,
    "type_declaration" TEXT,
    "date" DATE,
    "montant_ttc" INT,
    "detail" TEXT,
    "identifiant_convention" TEXT,
    "nom_prénom" TEXT,
    "montant_masque" TEXT
);
