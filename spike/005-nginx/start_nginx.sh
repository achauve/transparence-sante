#!/usr/bin/env bash

dir="$( cd "$(dirname "$0")" ; pwd -P )"

docker rm -f ts-nginx
docker run -d \
    --name ts-nginx \
    -p 80:80 \
    -v ${dir}/data:/data/ \
    -v ${dir}/nginx.conf:/etc/nginx/nginx.conf:ro \
    nginx:1.15-alpine
