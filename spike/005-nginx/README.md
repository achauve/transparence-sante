This spike is the result of nginx beginner tutorial [url](https://nginx.org/en/docs/beginners_guide.html) with nginx docker image [url](https://hub.docker.com/_/nginx/). 

The trickiest part was to comment the line `include /etc/nginx/conf.d/*.conf;` from the default configuration… 

When running `start-nginx.sh`, one should get result when connecting to `localhost` and `localhost/logo.png`.