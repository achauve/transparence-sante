#!/usr/bin/env bash


docker exec -it ts-nginx sh
cd /etc/nginx/
cat conf.d/default.conf | grep -v '#' | sed '/^\s*$/d'
