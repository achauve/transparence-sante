select indexname, indexdef from pg_indexes where tablename = 'declaration';

SELECT am.amname AS index_method,
       opc.opcname AS opclass_name,
       opc.opcintype::regtype AS indexed_type,
       opc.opcdefault AS is_default
    FROM pg_am am, pg_opclass opc
    WHERE opc.opcmethod = am.oid
    ORDER BY index_method, opclass_name;


show lc_collate;


SELECT pg_reload_conf();

SELECT * FROM pg_stat_activity ;

select indexname, indexdef from pg_indexes where tablename = 'declaration';

CREATE INDEX "declaration_lower_Nom_text_pattern_ops_Nom_index"ON public.declaration (lower("Nom") text_pattern_ops, "Nom");
CREATE INDEX "declaration_lower_Nom_text_pattern_ops_index" ON public.declaration (lower("Nom") text_pattern_ops);
CREATE INDEX "declaration_Nom_index" ON public.declaration ("Nom");

DROP INDEX public.declaration_lower_name_text_pattern_ops_name_index RESTRICT;




ANALYSE;

EXPLAIN  ANALYSE
  SELECT "public"."declaration"."Nom" AS "Nom" FROM "public"."declaration"
  WHERE (lower("public"."declaration"."Nom") like 'test%')
  GROUP BY "public"."declaration"."Nom"
  ORDER BY "public"."declaration"."Nom" ASC
;

ALTER TABLE public.declaration RENAME COLUMN "Nom" TO name;
ALTER INDEX "declaration_lower(""Nom"") text_pattern_ops_Nom_index" RENAME TO "declaration_lower_name_text_pattern_ops_name_index";


EXPLAIN  (ANALYSE, BUFFERS)
  SELECT name FROM declaration
  WHERE (lower(name) like 'test%');

EXPLAIN  (ANALYSE, BUFFERS)
  SELECT name FROM declaration
  WHERE (lower(name) = 'dupont');

CREATE INDEX "declaration_name text_pattern_ops_index" ON public.declaration (name text_pattern_ops);
CREATE INDEX "declaration_name_index" ON public.declaration (name);


EXPLAIN  (ANALYSE, BUFFERS)
  SELECT name FROM declaration
  WHERE (name = 'DUPONT')
;


EXPLAIN  (ANALYSE, BUFFERS)
  SELECT name FROM declaration
  WHERE (name like 'DUPONT%');

EXPLAIN  (ANALYSE, BUFFERS)
  SELECT count(*) FROM declaration
  WHERE (name like 'DUPONT%');

select relname, relkind, reltuples, relpages, relallvisible from pg_class where relname like 'declaration%';
select * from pg_settings where name = 'autovacuum' ;

SELECT *, attname, inherited, n_distinct,
       array_to_string(most_common_vals, E'\n') as most_common_vals
FROM pg_stats
WHERE tablename = 'declaration';


VACUUM ANALYSE;




ALTER TABLE public.declaration RENAME COLUMN name TO "Nom";

EXPLAIN  (ANALYSE, BUFFERS)
  SELECT count(*) FROM declaration
  WHERE ("Nom" like 'DUPONT%');



EXPLAIN (ANALYSE, BUFFERS)
  SELECT "public"."declaration"."Identifiant_Entreprise" AS "Identifiant_Entreprise", "public"."declaration"."Identifiant_Personne" AS "Identifiant_Personne", "public"."declaration"."Nom" AS "Nom", "public"."declaration"."Prénom" AS "Prénom", "public"."declaration"."benef_categorie" AS "benef_categorie", "public"."declaration"."benef_identifiant_type" AS "benef_identifiant_type", "public"."declaration"."benef_qualite" AS "benef_qualite", "public"."declaration"."benef_specialite" AS "benef_specialite", "public"."declaration"."date" AS "date", "public"."declaration"."entreprise_denomination_sociale" AS "entreprise_denomination_sociale", "public"."declaration"."montant_ttc" AS "montant_ttc", "public"."declaration"."type_declaration" AS "type_declaration"
    FROM "public"."declaration"
    WHERE "public"."declaration"."Nom" = 'GERARD'
    LIMIT 2000

EXPLAIN  ANALYSE
  SELECT "public"."declaration"."Nom" AS "Nom" FROM "public"."declaration" WHERE (lower("public"."declaration"."Nom") like 'gerard%') GROUP BY "public"."declaration"."Nom" ORDER BY "public"."declaration"."Nom" ASC LIMIT 100


CREATE INDEX "declaration_lower_name_index" ON public.declaration (lower("NOM_Prénom") text_pattern_ops, "NOM_Prénom");
CREATE INDEX "declaration_name_index" ON public.declaration ("NOM_Prénom");
VACUUM ANALYSE;
