/*
docker exec -it ts-postgres bash

# Install pg_cron
PG_CRON_VERSION=1.0.2
apk add --no-cache --virtual .build-deps build-base ca-certificates openssl tar \
    && wget -O /pg_cron.tgz https://github.com/citusdata/pg_cron/archive/v$PG_CRON_VERSION.tar.gz \
    && tar xvzf /pg_cron.tgz && cd pg_cron-$PG_CRON_VERSION \
    && sed -i.bak -e 's/-Werror//g' Makefile \
    && sed -i.bak -e 's/-Wno-implicit-fallthrough//g' Makefile \
    && make && make install \
    && cd .. && rm -rf pg_cron.tgz && rm -rf pg_cron-*


# Add shared_preload_libraries
cat /var/lib/postgresql/data/postgresql.conf | grep shared_preload_libraries

vi /var/lib/postgresql/data/postgresql.conf
#shared_preload_libraries = '' => shared_preload_libraries = 'pg_cron,pg_stat_statements'/

# OR one-liner with sed
sed -i "s/#shared_preload_libraries = \'\'/shared_preload_libraries = 'pg_cron,pg_stat_statements'/" /var/lib/postgresql/data/postgresql.conf

cat /var/lib/postgresql/data/postgresql.conf | grep shared_preload_libraries
exit

docker restart ts-postgres

# In case container does not start (because of startup error), modify its content starting another container
https://github.com/docker-library/postgres/issues/412
docker run -it --rm --volumes-from ts-postgres postgres:10.1-alpine bash

*/


CREATE SCHEMA monitor;
SHOW search_path;
SET search_path TO monitor, public;

-- Map filesystem info
-- https://aaronparecki.com/2015/02/19/8/monitoring-cpu-memory-usage-from-postgres

CREATE EXTENSION file_fdw;
CREATE SERVER fileserver
  FOREIGN DATA WRAPPER file_fdw;

CREATE FOREIGN TABLE loadavg
(one text, five text, fifteen text, scheduled text, pid text)
SERVER fileserver
OPTIONS (filename '/proc/loadavg', format 'text', delimiter ' ');

CREATE FOREIGN TABLE meminfo
(stat text, value text)
SERVER fileserver
OPTIONS (filename '/proc/meminfo', format 'csv', delimiter ':');

SELECT * FROM loadavg;
SELECT * FROM meminfo;

-- Extract metrics
CREATE TABLE metrics AS
  SELECT
    now(),
    substring(meminfo.value, ' *(\d*) kB') :: int as available_memory_kb,
    loadavg.one :: float                          as load_average_one
  FROM meminfo, loadavg
  WHERE meminfo.stat = 'MemAvailable';

SELECT * FROM metrics;

/*INSERT INTO metrics
  SELECT
    now(),
    substring(meminfo.value, ' *(\d*) kB') :: int as available_memory_kb,
    loadavg.one :: float as load_average_one
  FROM meminfo, loadavg
  WHERE meminfo.stat = 'MemAvailable';*/

-- Cron of the insert
-- https://github.com/citusdata/pg_cron/issues/17
-- https://github.com/citusdata/pg_cron
-- DROP EXTENSION IF EXISTS pg_cron;
CREATE EXTENSION pg_cron;

SELECT cron.schedule('* * * * *',
                     $$
INSERT INTO monitor.metrics
  SELECT
    now(),
    substring(meminfo.value, ' *(\d*) kB') :: int as available_memory_kb,
    loadavg.one :: float as load_average_one
  FROM monitor.meminfo, monitor.loadavg
  WHERE meminfo.stat = 'MemAvailable';
                     $$);

SELECT * FROM cron.job;

-- Create alert in metabase
-- https://www.metabase.com/docs/latest/users-guide/15-alerts.html


-- pg_stat_statements
-- https://www.postgresql.org/docs/current/static/pgstatstatements.html
CREATE EXTENSION pg_stat_statements;

select * FROM pg_stat_statements;

/*
-- Metabase:: userID: 1 queryType: MBQL queryHash: 96297fb5aabe8bf3b5b104b3752f4cfff759c03e2d952b47ad83c6d4fa81f5aa
SELECT count(*) AS "count"
FROM "public"."declaration"
*/

CREATE VIEW pg_stat_statements_metabase AS
  WITH tmp AS (
      SELECT
        substring(query, '-- Metabase:: userID: (\d+)') :: int   as metabase_user_id,
        substring(query, 'queryType: MBQL queryHash: .*\n(.*)$') as sql_query,
        calls,
        total_time,
        min_time,
        max_time,
        mean_time,
        rows,
        shared_blks_hit,
        shared_blks_read

      FROM pg_stat_statements
  )
  SELECT *
  FROM tmp
  WHERE metabase_user_id IS NOT NULL;

select *
from pg_stat_statements_metabase;

-- Extract usage metric by user


CREATE TABLE postgres_usage_by_user AS
  SELECT
    now(),
    sum(total_time) as total_user_time,
    metabase_user_id
  FROM monitor.pg_stat_statements_metabase
  GROUP BY metabase_user_id;


SELECT *
from postgres_usage_by_user;

SELECT cron.schedule('* * * * *',
                     $$
                     INSERT INTO monitor.postgres_usage_by_user
                       SELECT
                         now(),
                         sum(total_time) as total_user_time,
                         metabase_user_id
                       FROM monitor.pg_stat_statements_metabase
                       GROUP BY metabase_user_id;
                     $$);

SELECT *
FROM cron.job;

SELECT *
from postgres_usage_by_user;

--
CREATE VIEW monitor.hour_delta_by_user AS
  WITH
      active_user AS (
        SELECT metabase_user_id
        FROM monitor.pg_stat_statements_metabase
        GROUP BY metabase_user_id
    ),
      previous_hour_total_by_user AS (
        SELECT
          max(total_user_time) as total_user_time,
          metabase_user_id
        FROM monitor.postgres_usage_by_user
        WHERE now() - now > interval '1 hour'
        GROUP BY metabase_user_id
    ),
      previous_hour_total_by_user_with_default AS (
        SELECT
          COALESCE(total_user_time, 0) as total_user_time,
          active_user.metabase_user_id
        FROM active_user
          LEFT JOIN previous_hour_total_by_user
            ON active_user.metabase_user_id = previous_hour_total_by_user.metabase_user_id
    ),
      current_total_by_user AS (
        SELECT
          sum(total_time) as total_user_time,
          metabase_user_id
        FROM monitor.pg_stat_statements_metabase
        GROUP BY metabase_user_id
    ),
      delta_by_user_since_previous_hour AS (
        SELECT
          current.total_user_time - previous.total_user_time as delta_time_by_user,
          current.metabase_user_id
        FROM current_total_by_user as current
          JOIN previous_hour_total_by_user_with_default as previous
            ON current.metabase_user_id = previous.metabase_user_id
    )
  SELECT
    metabase_user_id,
    delta_time_by_user
  FROM delta_by_user_since_previous_hour
  WHERE delta_time_by_user > 0;

SELECT *
FROM monitor.hour_delta_by_user;
