Learning to use [Jekyll](https://jekyllrb.com/docs/quickstart/), with  [minimal-mistakes theme](https://mmistakes.github.io/minimal-mistakes/).

Creating a first version of website

Usage

    # To build and serve
    docker-compose up -d
    # Then
    docker-compose exec site jekyll build
    
    # To only build
    docker-compose run --rm site jekyll build
