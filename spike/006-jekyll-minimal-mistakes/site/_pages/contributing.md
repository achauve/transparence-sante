---
permalink: /contributing/
title: Contribuer
header:
  image: https://i2.wp.com/thecontextofthings.com/wp-content/uploads/2016/04/transparency-in-business.jpg?w=3872
  caption: "[Photo credit](http://thecontextofthings.com/2016/04/18/transparency-in-business/)"
---


## État du projet

EurosForDocs.fr est un projet récent, lancé à temps partiel depuis Mai 2018. 

C'est un projet [ouvert](https://gitlab.com/pajachiet/transparence-sante), basé sur des technologies open-source, 
initié dans l'intérêt des patients et du système de santé français.

Le projet a atteint une première version minimal viable en Août 2018. 
Pour croître et se péréniser, il s'agit désormais de constituer une communauté ouverte pour le faire progresser.

## Qui

Toute personne souhaitant contribuer au projet est la bienvenue.

Écrire à `contact@eurosfordocs.fr` en décrivant votre intérêt pour le projet. 
Toute remarque ou idée d'amélioration est bienvenue.  

## Quoi

Les chantiers prioritaires sont informatiques :
- Développement et design Web
- Nettoyage et analyses de données
- DevOps et Architecture
- Nettoyage du code pour simplifier au maximum les contributions
- Sécurité

Expertise thématique large : 
- Compléter les connaissances sur les relations industries-acteurs de santé
- Nouer des partenariats

Les compétences suivantes seront rapidement précieuses:
- Rédaction de contenu
- Communication
- Graphisme
- Droit 
- Audit
- Économie de la santé
- … 