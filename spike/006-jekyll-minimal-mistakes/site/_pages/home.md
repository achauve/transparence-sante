---
permalink: /
title: Bienvenue
layout: splash
date: 2016-03-23T11:48:41-04:00
header:
  overlay_color: "#000"
  overlay_filter: "0.2"
  overlay_image: http://www.eu-patient.eu/globalassets/images/news/trust-transparency.jpg?maxwidth=573
  caption: "[Photo credit](http://www.eu-patient.eu/News/News/transparency-is-our-legitimacy-currency-lets-protect-it/)"
excerpt: "Pour la transparence du lobbying des industries de santé."
---
