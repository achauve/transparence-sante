---
permalink: /explore/
title: Explorer
toc: true
header:
  image: /assets/images/metabase.png
  image_description: "Exemple d'usage de Metabase"
---

## Metabase

### Présentation

EurosForDocs.fr met à disposition l'outil [Metabase](https://www.metabase.com/) pour explorer les données de la base transparence santé. 

Ce puissant outil open-source permet de simplement explorer des données, d'enregistrer des analyses, et de les assembler sous la forme de tableaux de bord.

Lorsque ces analyses sont finalisées, elles peuvent être partagé sur le site public via un lien[^futur_embedding].

[^futur_embedding]: Pour l'instant, l'incorportaion se fait via un iframe. Lorsque le site aura un backend, il sera possible de mieux intégrer les visualisations aux pages, et de sécuriser l'accès. À terme, il sera souhaitable de servir des visualisations grand public sans passer par Metabase, afin de contrôler finement les possibilités et le rendu.

### Accès

Accéder à metabase à l'adresse suivante [www.eurosfordocs.fr/metabase](http://www.eurosfordocs.fr/metabase). 

Pour se connecter, demander l'ouverture d'un compte à `contact@eurosfordocs`.

Il est également possible de créer directement un compte via le `Sign In Google`, pour les adresses du domaine `@gmail.com`

### Découverte 

Pour découvrir l'outil, nous vous invitons à parcourir la [documentation](https://www.metabase.com/docs/latest/getting-started.html).

Commencer par explorer les [dashboards](http://www.eurosfordocs.fr/metabase/dashboards) déjà existant. Le stockage des données est optimisé pour servir rapidement ces dashboards.

S'essayer ensuite à poser des Questions "Custom", via les options de filtre et de regroupement.   


## Données

### Stockage et lenteur des requêtes personalisées

Les données sont téléchargées, nettoyées, stockées dans une base de données PostgreSQL, puis indexées et aggrégées afin d'accélérer les requêtes.

Lorsqu'une requête personnalisée est créée, elle ne peut généralement pas s'appuyer sur un index ou une vue pré-aggrégées. La base de données doit alors typiquement parcourir toutes les lignes, soit ~2.5 Go, ce qui prend logiquement plus de 30 secondes. Des solutions techniques pour accélérer les requêtes personnalisées pourront être mise en oeuvre à l'avenir.

Par ailleurs, il faut noter que le serveur hébergeant EurosForDocs.fr est actuellement de petite capacité.  

### Une table pour chaque fichier csv

Les données brutes publiées sur le site [Data Gouv](https://www.data.gouv.fr/fr/datasets/transparence-sante-1/) présente 4 fichiers csv:

- un annuaire des entreprises ayant effectué une déclaration
- 3 fichiers de déclarations: 
    - **conventions** qui liste les contrats
    - **rémunérations** versées en contrepartie d'une prestation
    - **avantage** qui liste les cadeaux offerts sans contrepartie 

Chacun de ces fichier est nettoyé, puis ingéré dans une table séparée de la base PostgreSQL, respectivement appelées **entreprise**, **declaration_convention**, **declaration_avantage** et **declaration_remuneration**. 

### Une table pour le fichier RPPS

L'annuaire RPPS est chargé dans la base PostgreSQL. L'objectif est d'utiliser ensuite cette annuaire pour corriger les noms de bénéficiaires déclarés par les entreprises.

### Des vues additionnelles

Une vue **declaration** reprend les colonnes communes des 3 tables de déclarations, pour simplifier des analyses communes.

Des vues matérialisées **declaration_aggregate_montant_ttc_by_entreprise_émmetrice** et **declaration_aggregate_montant_ttc_by_nom_prénom** stockent de façon aggrégée les montants de déclaration par entreprise et par bénéficiaire, afin de réponde immédiatement aux requêtes correspondantes (Dashobards "Top").
