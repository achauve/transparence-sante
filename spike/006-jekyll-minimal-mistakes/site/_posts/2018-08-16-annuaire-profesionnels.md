---
---

Cette page permet de chercher dans le Répertoire Partagé des Professionnels de Santé ([RPPS](https://www.ameli.fr/medecin/exercice-liberal/vie-cabinet/installation-liberal/rpps)). 

L'identifiant RPPS présent dans ce répertoire est très souvent absent ou incorrect dans les déclarations de la base Transparence-Santé.

Un important travail d'analyse de données consiste à rétablir un lien entre chaque déclaration et une entrée du RPPS. 

<iframe 
    src="http://www.eurosfordocs.fr/metabase/public/dashboard/768c0597-bea8-40e1-84f8-bb9252256b89" 
    frameborder="0" 
    width="1200" 
    height="1000" 
    allowtransparency
></iframe>
