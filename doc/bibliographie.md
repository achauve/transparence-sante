# Projets similaires par Pays

## US 

### Gouvernement

- Open Payments 
[url](https://openpaymentsdata.cms.gov/)

- Exemple sur un médecin [url](https://openpaymentsdata.cms.gov/physician/354917/summary), avec comparaison sur ProPublica [url](https://projects.propublica.org/docdollars/doctors/pid/354917)

### ProPublica

- Dollars for Docs 
[viz](https://projects.propublica.org/docdollars/),
[articles](https://www.propublica.org/series/dollars-for-docs)

- What Percentage of Doctors at Your Hospital Take Drug, Device Payments? 
[viz](https://projects.propublica.org/graphics/d4d-hospital-lookup), 
[article](https://www.propublica.org/article/drug-device-makers-find-receptive-audience-at-for-profit-southern-hospitals)

- Now There’s Proof: Docs Who Get Company Cash Tend to Prescribe More Brand-Name Meds [url](https://www.propublica.org/article/doctors-who-take-company-cash-tend-to-prescribe-more-brand-name-drugs)

- Interview du fondateur de ProPublica sur France Culture -
Paul Steiger : "L'important, c'est que nos enquêtes aient un impact sur la société" 
[url](https://www.franceculture.fr/medias/paul-steiger-limportant-cest-que-nos-enquetes-aient-un-impact-sur-la-societe)

## Allemagne 

### CORRECT!V

- Euros for docs : Find your doctor 
[viz](https://correctiv.org/en/investigations/euros-doctors/database/)

- 20161215
Doctors eagerly welcome big pharma's money 
[article](https://correctiv.org/en/investigations/euros-doctors/articles/2016/12/15/embracing-millionsbe-embraced/)

## France 

### Gouvernement

- La base de données publique Transparence - Santé
[url](https://www.transparence.sante.gouv.fr)

- 2017-07-01 - 
Consultation des déclarations publiques d'intérets
[url](https://dpi.sante.gouv.fr/dpi-public-webapp/app/consultation/accueil)

- Archive DPI ANSM 
[url](https://icfidnet.ansm.sante.fr/Public/archive_dpi.php)

### Regards Citoyens

- 2015 
Lumière sur Sunshine - Ce que les labos donnent à nos médecins
[url](https://www.regardscitoyens.org/sunshine/),
[code source](http://github.com/regardscitoyens/sunshine-data)


## Autres Europe

- 2017-10-23 - Civio - 
Article comparatif des lois européennes, avec analyse de données
[url](https://civio.es/medicamentalia/2017/10/23/english-and-swiss-doctors-more-transparent-than-german-and-spanish-ones/)

- 2017-05 - Health Action International -
The Sun Shines on Europe: Transparency of financial relationships in the healthcare sector
[url](http://haiweb.org/wp-content/uploads/2017/03/Sunshine-Act.pdf)


- 2012 - European Court of auditors -
Management of conflict of interest in selected EU Agencies
[url](https://www.eca.europa.eu/Lists/News/NEWS1210_11/NEWS1210_11_EN.PDF)

# Articles, rapports

## État

### Cour des comptes

- 2016-03-23
La prévention des conflits d’intérêts en matière d’expertise sanitaire
[url](https://www.ccomptes.fr/fr/publications/la-prevention-des-conflits-dinterets-en-matiere-dexpertise-sanitaire),
[pdf](bibliographie_pdf/Etat/20160323-prevention-conflits-interets-en-matiere-expertise-sanitaire.pdf)

### Sénat

- 2011-06-28 
La réforme du système du médicament, enfin
[url](http://www.senat.fr/rap/r10-675-1/r10-675-18.html),
[pdf](bibliographie_pdf/Etat/20110628_La%20réforme%20du%20système%20du%20médicament,%20enfin%20.pdf)

    - Rapport d'information de Mme Marie-Thérèse HERMANGE, fait au nom de la Mission commune d'information sur le Mediator
    - Chapitres spécifiques
        - L'indépendance de l'expertise sanitaire en question
[url](http://www.senat.fr/rap/r10-675-1/r10-675-19.html#toc245)

        - Mieux assurer la transparence
[url](http://www.senat.fr/rap/r10-675-1/r10-675-110.html#toc282)
        
            *Proposition n° 6 :  Mettre en place un registre public des avantages consentis par l'industrie du médicament aux professionnels de santé géré par l'Autorité de déontologie de la vie publique*

### Direction générale de la santé

- 2008-06
Rapport sur l'indépendance et la valorisation de l'expertise venant à l'appui des décisions en santé publique
[url](http://www.ladocumentationfrancaise.fr/rapports-publics/094000044/index.shtml), [pdf](http://solidarites-sante.gouv.fr/IMG/pdf/Rapport_expertise_sante_publique_2008.pdf)


## Ordres

### Conseil national de l'ordre des médecins

- 2016-03-30
L'Ordre rappelle l'impératif de mettre en oeuvre le décret d'application permettant de contrôler toutes les déclarations d'intéret
[url](https://www.conseil-national.medecin.fr/node/1704)

- 2013-05-25
Décret sur la publication des liens d’intérêt et la transparence : nous sommes très loin du compte
[url](https://www.conseil-national.medecin.fr/article/decret-sur-la-publication-des-liens-d%E2%80%99interet-et-la-transparence-nous-sommes-tres-loin-du-compte-1325)

- 2012-05-03
Sécurité du médicament - des patients mieux protégés
[url](https://pdfhall.com/medecins-n24-conseil-national-de-lordre-des-medecins_59f532fa1723dd827e801938.html)

- 2012-10-23
Publication des liens d'intérêt : de la lumière à l'obscurité
[url](https://www.conseil-national.medecin.fr/article/publication-des-liens-d-interet-de-la-lumiere-l-obscurite-1265)

- 2011-05-03
Indépendance professionnelle des médecins – Déclaration d’intérêts : « Sunshine act » à la française
[url](https://www.conseil-national.medecin.fr/article/independance-professionnelle-des-medecins-%E2%80%93-declaration-d%E2%80%99interets-%C2%AB-sunshine-act-%C2%BB-la-francaise-1076)

- 2011-10-04
Conflits d’intérêts : pour restaurer la confiance l’Ordre préconise des mesures réglementaires et législatives
[url](https://www.conseil-national.medecin.fr/article/conflits-d%E2%80%99interets-pour-restaurer-la-confiance-l%E2%80%99ordre-preconise-des-mesures-reglementaires-et-legi-1114)

### Ordre national des pharmaciens

- Dispositif anti-cadeaux
[url](http://www.ordre.pharmacien.fr/Nos-missions/Le-role-de-l-Ordre-dans-les-missions-de-sante-publique/Dispositif-anti-cadeaux)

- Transparence
[url](http://www.ordre.pharmacien.fr/Nos-missions/Le-role-de-l-Ordre-dans-les-missions-de-sante-publique/Transparence)


## Organisation indépendantes

### Revue Prescrire

- La Charte "Non merci…" [url](http://www.prescrire.org/Fr/12/38/0/2545/About.aspx)

- 2006-12
Conflits d’intérêts à l’Agence française des produits de santé : il reste beaucoup à faire
[url](http://www.prescrire.org/Fr/SummaryDetail.aspx?IssueId=278),
[pdf](bibliographie_pdf/Prescrire/200612_conflits_d'interets_a_l'Agence_francaise_des_produits_de_sante___il_reste_beaucoup_a_faire.pdf)


- 2010-03-01 
    - Conflits d'intérêts à l'Agence française des produits de santé (Afssaps)
[url](http://www.prescrire.org/Fr/3/31/24217/0/NewsDetails.aspx)

    - Valoriser l'expertise pour la rendre indépendante
[url](http://www.prescrire.org/Fr/3/31/24221/0/NewsDetails.aspx)

- 2015-05 
Liens d'intérêt : lever le voile
[url](http://www.prescrire.org/fr/3/31/49933/0/NewsDetails.aspx),
[pdf](bibliographie_pdf/Prescrire/201506_lever_le_voile.pdf)


### Formindep

Note : Ne pas rester sur l'onglet Actualité. Les autres onglets contiennent une excellente compilation d'informations.

- Indépendance des acteurs de santé 
[url](http://formindep.fr/independance-des-acteurs-de-sante/)

- Stratégies d'influence
[url](http://formindep.fr/strategies-dinfluence/)

## Journaux

### Le Monde
- 2013-04-18 
    - Médecine : témoignages d'incorruptibles
 [url](https://abonnes.lemonde.fr/sciences/article/2013/04/18/temoignages-de-ceux-qui-disent-non-aux-labos_3162094_1650684.html), 
 [pdf](bibliographie_pdf/Le_Monde/20130418_Médecine%20-%20témoignages%20d'incorruptibles.pdf)

    - Médecine : ceux qui disent non aux labos
[url](https://abonnes.lemonde.fr/sciences/article/2013/04/18/medecine-les-incorruptibles_3162091_1650684.html),
[pdf](bibliographie_pdf/Le_Monde/20130418_Médecine%20_%20ceux%20qui%20disent%20non%20aux%20labos.pdf)    
 

- 2017-10-12 
    - Comment l’argent des labos irrigue le monde de la santé
[url](https://abonnes.lemonde.fr/economie/article/2017/10/12/les-laboratoires-aux-petits-soins-pour-les-medecins_5199912_3234.html),
[pdf](bibliographie_pdf/Le_Monde/20171012_Comment%20l’argent%20des%20labos%20irrigue%20le%20monde%20de%20la%20santé.pdf) 
    
    -  Les ratés de la base de données publique Transparence Santé
[url](https://abonnes.lemonde.fr/les-decodeurs/article/2017/10/12/les-rates-de-la-base-de-donnees-publique-transparence-sante_5199937_4355770.html),
[pdf](bibliographie_pdf/Le_Monde/20171012_Les%20ratés%20de%20la%20base%20de%20données%20publique%20Transparence%20Santé.pdf) 

## Recherche universitaire

- Jérôme Greffion - Sociologue 
    -  2014 - Thèse de doctorat en sociologie - 
    Faire passer la pilule : visiteurs médicaux et entreprises pharmaceutiques face aux médecins : une relation socio-économique sous tensions privées et publiques (1905-2014) 
[url](http://www.theses.fr/2014EHES0133)

### Divers

#### Multimédia

- 2015-05-16 - #DATAGUEULE 37 -
Maladies à vendre 
[vidéo](https://www.youtube.com/watch?v=aOPW6wzs8Ks)


- 2016-05-12 - France Inter -
Santé : en finir avec les conflits d’intérêts
[podcast](https://www.franceinter.fr/personnes/anne-chailleu)


- 2016 - La fille de Brest
[url](https://fr.wikipedia.org/wiki/La_Fille_de_Brest)




#### Journaux / Blog

- 2016-04-03 - BMI System -
Le renforcement du pouvoir du régulateur dans le contrôle de l’application du dispositif anti-cadeaux : les apports de la loi de santé du 26 janvier 2016
[url](https://www.bmi-system.com/le-renforcement-du-pouvoir-du-regulateur-dans-le-controle-de-lapplication-du-dispositif-anti-cadeaux-les-apports-de-la-loi-de-sante-du-26-janvier-2016/)

- 2017-07-05 - EDP-Audio -
Anti-cadeaux, Transparence : des lois pour encadrer les liens d’intérêt
[url](https://www.edp-audio.fr/focus/5444-anti-cadeaux-transparence-des-lois-pour-encadrer-les-liens-d-interet)

- 2017-01-03 - l'Humanité - 
Santé. Contre les conflits d’intérêts, une transparence à petits pas
[url](https://www.humanite.fr/sante-contre-les-conflits-dinterets-une-transparence-petits-pas-629574)

- 2016-09-08 - Le quotidient du médecin -
Prix des médicaments : les vérités décoiffantes de Noël Renaudin, ex-patron du CEPS
[url](https://www.lequotidiendumedecin.fr/actualites/article/2016/09/08/prix-des-medicaments-les-verites-decoiffantes-de-noel-renaudin-ex-patron-du-ceps_824811)

- 2015-01 - Le Monde Diplomatique
Les dessous de l’industrie pharmaceutique
[url](https://www.monde-diplomatique.fr/2015/01/RAVELLI/51928)

- 2013-01-19 - Farfadoc - 
 Déclaration de conflits d’intérêts (humour)
[url](https://farfadoc.wordpress.com/2013/01/19/declaration-de-conflits-dinterets/)

- 2009-04 - Booksmag -
Le scandale de l'industrie pharmaceutique
[url](http://www.retrouversonnord.be/Le_scandale_pharmaceutique.pdf),

- Le marketing pharmaceutique
[url](http://lesmarketing.fr/le-marketing-pharmaceutique-lindustrie-pharmaceutique/)

# Loi

-  Loi anti-cadeau
[url](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000006688678&dateTexte=&categorieLien=cid)

- 2017-01-19 - 
Ordonnance n° 2017-49 relative aux avantages offerts par les personnes fabriquant ou commercialisant des produits ou des prestations de santé 
[url](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033893406&categorieLien=id)

- 2016-12-28 - 
Décret n° 2016-1939 relatif à la déclaration publique d'intérêts prévue à l'article L. 1451-1 du code de la santé publique et à la transparence des avantages accordés par les entreprises produisant ou commercialisant des produits à finalité sanitaire et cosmétique destinés à l'homme 
[url](https://www.legifrance.gouv.fr/eli/decret/2016/12/28/AFSX1637582D/jo/texte)
