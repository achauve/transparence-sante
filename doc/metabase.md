# Definition

This document explain how to setup Metabase for the project.

Replace METABASE_URL with appropriate value (ex  http://localhost:3000) 

## Settings

- METABASE_URL/admin/settings/general
    - "FRIENDLY TABLE AND FIELD NAMES" -> "Only replace underscores"


## Data model

- METABASE_URL/admin/datamodel/database
    - column to filter -> Category

## Question

- Question with raw data on most important columns          
        
## Dashboard

- Simple dashboard with filter on raw data

