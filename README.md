*This project aims to expose a cleaned version of [Transparence Santé](www.transparence.sante.gouv.fr) dataset.
It will supports [EurosForDocs.fr](http://www.eurosfordocs.fr/) website, as inspired by ProPublica's [DollarsForDocs](https://projects.propublica.org/docdollars) project.*
 
Ce projet a pour objectif d'exposer une version nettoyée de la base de données [Transparence-Santé](www.transparence.sante.gouv.fr), de façon similaire au site [DollarsForDocs](https://projects.propublica.org/docdollars) créé par ProPublica.


## Contexte

Suite au scandale sanitaire de l'[affaire du Mediator](https://fr.wikipedia.org/wiki/Affaire_du_Mediator), 
la loi Bertrand vise à prévenir les conflits d'intérêts et à renforcer l'indépendance de l’expertise sanitaire.

Elle impose aux industriels de santé de déclarer les liens d'intérêts qu'ils entretiennent avec les 
médecins, hopitaux, sociétés savantes, associations de malades, et autres acteurs de santé.

La base de donnée Transparence-Santé centralise ainsi 13 millions d'entrées, décrivant les cadeaux offerts, les rémunérations versées et les contrats passés. 

Ces données sont malheureusement difficiles d'accès:

- Le moteur de recherche public expose des déclarations hétérogènes, sans synthèse graphique.  
- Les données brutes disponibles en [téléchargement](https://www.data.gouv.fr/fr/datasets/transparence-sante-1/)
sont trop volumnineuses pour les logiciels d'analyse grand public.

## Objectif

Bien qu'incomplète, les déclarations d'intérêts mettent en lumière un système d'influence généralisé.

L'objectif de ce projet est de les rendre accessibles à tous : 
- patients - connaître les liens d'intérêts de leur médecins, 
- citoyens - comprendre les enjeux d'influence du système de santé auquel ils contribuent, 
- professionnels de santé - participer à l'indépendance de la médecine, 
- administrations publiques - contrôler la véracité des déclarations des industriels et l'indépendance des experts sanitaires, 
- journalistes et chercheurs - disposer d'une base de données et d'outils pour mener à bien leurs investigations et recherches

Ce projet  est au service d'une transparence réelle, 
socle fondamental pour progressivement rétablir une pratique médicale 
au service des patients, économe, et indépendante d'intérêts économiques privés.
 

## Concrètement

Ce projet comprend 3 enjeux techniques:

1. nettoyer la base de données source
1. offrir un outil d'analyse avancé aux experts
1. créer un site web grand public inspiré de DollarsForDocs


## État d'avancement

L'architecture principale du projet est en place, avec des fonctionnalités de nettoyage basiques, et une instance Metabase en ligne

Principaux chantiers :
- Faciliter la collaboration au projet
    - documenter le code, l'architecture, 
    - terminer le refactoring du code
    - améliorer la couverture de tests
    - mettre en place une intégration continue
    - compléter et réorganiser la [bibliographie](doc/bibliographie.md)
- Ajouter des procédures de nettoyages de données
    - harmonisation des catégories
    - nettoyage des valeurs abberrantes
    - identification de bénéficiaires uniques (personnes morales & physiques)
    - groupement des entreprises déclarantes  (ex Sanofi)
    - etc
- Réaliser des analyses thématiques
    - déclarations illégales
    - distribution géographique, par spécialité
    - etc.
- Assurer la qualité de service Metabase
    - meilleure gestion du cache pour chargement initial des dashboards
    - connexion https sur eurosfordocs.fr
- Développer un site grand public

## Comment contribuer

Ce projet a besoin de votre aide:
- de vos critiques constructives
- de votre expérience du sujet
- de vos compétences techniques, notamment pour le nettoyage des données et les analyses thématiques, et bien plus si possibilité ! 
- de vos idées, initiatives, contacts

Écrire à contact@eurosfordocs.fr