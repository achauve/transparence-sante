#!/usr/bin/env bash

FOLDER=backup/`date +%Y-%m-%d:%H:%M:%S`

echo Saving metabase data to ${FOLDER}
mkdir -p ${FOLDER}
scp root@ts:transparence-sante/docker/metabase/data/metabase.db.mv.db ${FOLDER}/