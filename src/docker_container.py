import os

import docker

from config.database import POSTGRES_PORT
from config.secrets import POSTGRES_PASSWORD
from src.constants.directories import DOCKER_METABASE_DIR, DOCKER_POSTGRES_DIR, DOCKER_NGINX_DIR, WEBSITE_DIR
from src.utils import get_confirmation

PROJECT_PREFIX = "ts-"

NETWORK_NAME = PROJECT_PREFIX + "network"

POSTGRES_IMAGE = 'ts-postgres:latest'
POSTGRES_CONTAINER = PROJECT_PREFIX + "postgres"
POSTGRES_VOLUMES = {
    os.path.join(DOCKER_POSTGRES_DIR, "data"): {
        'bind': "/var/lib/postgresql/data",
        'mode': 'rw'
    },
    os.path.join(DOCKER_POSTGRES_DIR, "postgresql.conf"): {
        'bind': "/var/lib/postgresql/data/postgresql.conf",
        'mode': 'rw'
    }
}

METABASE_IMAGE = 'metabase/metabase:v0.29.3'
METABASE_CONTAINER = PROJECT_PREFIX + "metabase"
METABASE_PORT = 3000
METABASE_VOLUMES = {
    os.path.join(DOCKER_METABASE_DIR, "data"): {
        'bind': "/metabase-data/metabase.db",
        'mode': 'rw'
    }
}
METABASE_ENVIRONMENT = dict(
    MB_DB_FILE="/metabase-data/metabase.db",
    MAPBOX_API_KEY="TODO",
)

NGINX_IMAGE = 'nginx:1.14'
NGINX_CONTAINER = PROJECT_PREFIX + "nginx"
NGINX_PORT = 80
NGINX_VOLUMES = {
    os.path.join(DOCKER_NGINX_DIR, "nginx.conf"): {
        'bind': "/etc/nginx/nginx.conf",
        'mode': 'ro'
    },
    # os.path.join(DOCKER_NGINX_DIR, "data", "nginx-cache"): {
    #     'bind': "/var/cache/nginx",
    #     'mode': 'rw'
    # },
    # os.path.join(DOCKER_NGINX_DIR, "data", "nginx-pid"): {
    #     'bind': "/var/run",
    #     'mode': 'rw'
    # },
    os.path.join(DOCKER_NGINX_DIR, "data", "letsencrypt"): {
        'bind': "/etc/letsencrypt",
        'mode': 'rw'
    },
    # os.path.join(DOCKER_NGINX_DIR, "data", "www"): {
    #     'bind': "/data/www",
    #     'mode': 'rw'
    # },
    WEBSITE_DIR: {
        'name': 'website',
        'bind': "/data/www",
        'mode': 'rw'
    },
}

CONTAINER_NAMES = [POSTGRES_CONTAINER, METABASE_CONTAINER, NGINX_CONTAINER]
client = docker.from_env()


def reset(ask_confirmation=True) -> None:
    if ask_confirmation:
        confirmation = get_confirmation(
            "You are going to remove all Docker containers and images from this project.\n"
            "Container data will be kept as it mapped on host.\n"
            "Re-building docker images make take a long time.\n"
            "Do you confirm ?")
    else:
        confirmation = True

    if confirmation:
        reset_containers()
        remove_image(POSTGRES_IMAGE)
    else:
        print("Aborting Docker reset.")


def setup() -> None:
    create_network()
    build_postgres_image()
    for container_name in CONTAINER_NAMES:
        create_container(container_name)


def start() -> None:
    for container_name in CONTAINER_NAMES:
        start_container(container_name)


def reset_containers():
    for container_name in CONTAINER_NAMES:
        remove_container(container_name)


def remove_container(container_name) -> None:
    print("Force removing container '{}".format(container_name))
    container = get_container(container_name)
    if container is not None:
        container.remove(force=True)


def remove_image(image_name) -> None:
    print("Force removing image '{}".format(image_name))
    client.images.remove(
        image=image_name,
        force=True
    )


def create_network() -> None:
    if client.networks.list(filters={'name': NETWORK_NAME}):
        return None
    else:
        client.networks.create(NETWORK_NAME)


def build_postgres_image():
    if get_image(POSTGRES_IMAGE):
        print("Docker image '{}' already exists".format(POSTGRES_IMAGE))
    else:
        print("Building postgres docker image '{}'".format(POSTGRES_IMAGE))
        client.images.build(
            path=DOCKER_POSTGRES_DIR,
            tag=POSTGRES_IMAGE,
            rm=True,
        )
        print("-- Finished building.")


def create_container(container_name: str) -> None:
    if get_container(container_name):
        print("Docker container {} already exists".format(container_name))
    else:
        if container_name == POSTGRES_CONTAINER:
            pull_image(POSTGRES_IMAGE)
            print("Creating Docker container {}".format(POSTGRES_CONTAINER))
            client.containers.create(
                network=NETWORK_NAME,
                image=POSTGRES_IMAGE,
                name=POSTGRES_CONTAINER,
                ports={POSTGRES_PORT: POSTGRES_PORT},
                volumes=POSTGRES_VOLUMES,
                environment={'POSTGRES_PASSWORD': POSTGRES_PASSWORD},
            )
        if container_name == METABASE_CONTAINER:
            pull_image(METABASE_IMAGE)

            print("Creating Docker container {}".format(METABASE_CONTAINER))
            client.containers.create(
                network=NETWORK_NAME,
                image=METABASE_IMAGE,
                name=METABASE_CONTAINER,
                ports={METABASE_PORT: METABASE_PORT},
                volumes=METABASE_VOLUMES,
                environment=METABASE_ENVIRONMENT,
            )
        if container_name == NGINX_CONTAINER:
            pull_image(NGINX_IMAGE)

            print("Creating Docker container {}".format(NGINX_CONTAINER))
            client.containers.create(
                network=NETWORK_NAME,
                image=NGINX_IMAGE,
                name=NGINX_CONTAINER,
                ports={NGINX_PORT: NGINX_PORT},
                volumes=NGINX_VOLUMES,
                read_only=False,
            )


def get_container(container_name: str):
    all_containers = client.containers.list(all=True)

    # Manually filter container.name to match exact name.
    # containers.list(all=True, filters={'name': container_name}) match substrings
    for container in all_containers:
        if container.name == container_name:
            return container

    return None


def get_image(image_name: str):
    try:
        return client.images.get(image_name)
    except docker.errors.ImageNotFound:
        return None


def pull_image(image_name) -> None:
    if not client.images.list(name=image_name):
        print("Pulling Docker image {}".format(image_name))
        client.images.pull(image_name)
        print("-- Finished pulling")


def start_container(container_name: str) -> None:
    container = get_container(container_name)
    print("Container '{}' has status '{}'".format(container_name, container.status))
    if container.status != "running":
        print("Starting container '{}'".format(container_name))
        container.start()
        print("-- Started")


if __name__ == '__main__':
    # reset()
    remove_container(NGINX_CONTAINER)
    setup()
    start()
