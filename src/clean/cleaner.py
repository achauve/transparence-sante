import os

import pandas as pd
import pandas_profiling

from config.read_csv import CHUNK_SIZE, ROWS_LIMIT
from src.constants.csv import SEMICOLON_SEPARATOR
from src.constants.directories import raw_path, cleaned_path
from src.utils import prefix
from src.utils import timeit

PROFILE_REPORT_DIR_PATH = os.path.join('reports', 'raw', 'profile')


class Cleaner:
    def __init__(self, csv_name, rename_columns, assign=None, drop_columns=None, validators=None,
                 raw_separator=SEMICOLON_SEPARATOR):
        self.csv_name = csv_name
        self.raw_separator = raw_separator
        self.cleaned_separator = SEMICOLON_SEPARATOR
        self.raw_csv_path = raw_path(self.csv_name)
        self.cleaned_csv_path = cleaned_path(self.csv_name)

        self.rename_columns = rename_columns or dict()
        self.assign = assign or dict()
        self.drop_columns = drop_columns or list()

        self.validators = validators or list()

    def profile_raw_csv(self):
        report_path = os.path.join(PROFILE_REPORT_DIR_PATH, prefix(self.csv_name) + '.html')

        print("Profiling raw '{}'. A report will be created in file '{}'".format(self.csv_name, report_path))
        df = pd.read_csv(self.cleaned_csv_path, sep=self.raw_separator)
        profile_report = pandas_profiling.ProfileReport(df, check_correlation=False)
        profile_report.to_file(report_path)

    def print_base_rename_columns_dict(self):
        print("Print a default rename column dict for {}".format(self.csv_name))
        csv_path = raw_path(self.csv_name)
        df = pd.read_csv(csv_path, sep=self.raw_separator, nrows=1)
        print('{')
        for column in df.columns:
            print("""    "{column}": "{column}",""".format(column=column))
        print('}')

    @timeit
    def run(self):
        print("Cleaning", self.csv_name)
        print("Cleaned file will have the following columns : {}".format(self._cleaned_csv_columns))
        if os.path.exists(self.cleaned_csv_path):
            os.remove(self.cleaned_csv_path)

        write_header = True
        for raw_df_chunk in self._raw_df_chunk_iterator():
            cleaned_df = self._clean_raw_df(raw_df_chunk)
            self._validate(cleaned_df)
            cleaned_df.to_csv(self.cleaned_csv_path, index=False, sep=self.cleaned_separator, mode='a',
                              header=write_header)

            write_header = False

    def _raw_df_chunk_iterator(self):
        return pd.read_csv(self.raw_csv_path, dtype='str', sep=self.raw_separator,
                           usecols=self.rename_columns.keys(),
                           chunksize=CHUNK_SIZE,
                           nrows=ROWS_LIMIT)

    def _clean_raw_df(self, raw_df_chunk):
        return (raw_df_chunk
                .rename(columns=self.rename_columns)
                .assign(**self.assign)
                .drop(columns=self.drop_columns)
                )

    @property
    def _cleaned_csv_columns(self):
        columns = set(self.rename_columns.values())
        columns |= self.assign.keys()
        columns -= set(self.drop_columns)
        return list(columns)

    def _validate(self, df):
        for is_valid in self.validators:
            assert is_valid(df), "'{}' was false for a cleaned dataframe chunk.".format(is_valid.__name__)

