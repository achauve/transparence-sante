from src.clean.declaration.declaration_cleaner import DeclarationCleaner
from src.constants.column_names import DATE, AMOUNT, CONVENTION_ID, DETAIL
from src.constants.csv import REMUNERATION_CSV

cleaner = DeclarationCleaner(
    csv_name=REMUNERATION_CSV,
    rename_columns={
        'remu_date': DATE,
        'remu_montant_ttc': AMOUNT,
        'remu_convention_liee': CONVENTION_ID
    },
    assign={
        DETAIL: None
    },
    drop_columns=list()
)
