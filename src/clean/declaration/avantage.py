from src.clean.declaration.declaration_cleaner import DeclarationCleaner
from src.constants.column_names import DATE, AMOUNT, CONVENTION_ID, DETAIL
from src.constants.csv import AVANTAGE_CSV

cleaner = DeclarationCleaner(
    csv_name=AVANTAGE_CSV,
    rename_columns={
        'avant_date_signature': DATE,
        'avant_montant_ttc': AMOUNT,
        'avant_convention_lie': CONVENTION_ID,
        'avant_nature': DETAIL,
    },
    assign=dict(),
    drop_columns=list()
)
