from src.clean.cleaner import Cleaner
from src.constants.column_names import ADVANTAGE, CONVENTION, REMUNERATION, DECLARATION_TYPE, NAME, SURNAME, \
    PHYSICAL_PERSON_ID, ISSUING_COMPANY_NAME, CATEGORY, ID_TYPE, SPECIALTY, PROFESSION, NAME_SURNAME, AMOUNT, \
    IS_AMOUNT_MASKED, DATE
from src.utils import merge_dict

MINIMAL_YEAR = 1970
MAXIMAL_YEAR = 2020
MINIMAL_YEAR_DECADE = 70
MAXIMAL_YEAR_DECADE = 20

# Rename
RENAME_COLUMNS = {
    'benef_nom': NAME,
    'benef_prenom': SURNAME,
    'benef_identifiant_valeur': PHYSICAL_PERSON_ID,
    'denomination_sociale': ISSUING_COMPANY_NAME,
    'categorie': CATEGORY,
    'identifiant_type': ID_TYPE,
    'benef_speicalite_libelle': SPECIALTY,
    'qualite': PROFESSION,
    'ligne_type': DECLARATION_TYPE
}


# Assign before
def declaration_type(df):
    declaration_code_wording_map = {
        "[A]": ADVANTAGE,
        "[C]": CONVENTION,
        "[R]": REMUNERATION
    }
    return df[DECLARATION_TYPE].map(declaration_code_wording_map)


def recipient_name_surname(df):
    return df[NAME].str.upper() + ' ' + df[SURNAME].str.title()


ASSIGN_BEFORE_SPECIFIC = {
    DECLARATION_TYPE: declaration_type,
    NAME_SURNAME: recipient_name_surname,
}


# Assign after
def is_amount_masked(df):
    return df[AMOUNT].isnull()


def corrected_date(df):
    def corrected_year(year):
        year_decade = year[2:]
        if MINIMAL_YEAR_DECADE < int(year_decade):
            return '19' + year_decade
        elif int(year_decade) < MAXIMAL_YEAR_DECADE:
            return '20' + year_decade
        else:
            return '2000'

    s_date = df[DATE]
    s_day_month = s_date.str[:6]
    s_year = s_date.str[6:].map(corrected_year)
    return s_day_month + s_year


ASSIGN_AFTER_SPECIFIC = {
    IS_AMOUNT_MASKED: is_amount_masked,
    DATE: corrected_date
}

# Drop
DROP_COLUMNS = [NAME, SURNAME]


# Validate
def is_year_in_correct_range(df):
    def is_date_string_year_in_correct_range(date_string):
        year = int(date_string[6:])
        return MINIMAL_YEAR <= year <= MAXIMAL_YEAR

    return df[DATE].map(is_date_string_year_in_correct_range).all()


VALIDATORS = [is_year_in_correct_range]


class DeclarationCleaner(Cleaner):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.rename_columns = merge_dict([RENAME_COLUMNS, self.rename_columns])
        self.assign = merge_dict([ASSIGN_BEFORE_SPECIFIC, self.assign, ASSIGN_AFTER_SPECIFIC])
        self.drop_columns = DROP_COLUMNS + self.drop_columns
        self.validators = VALIDATORS + self.validators
