import pandas as pd

from src.clean.declaration.declaration_cleaner import DeclarationCleaner
from src.constants.column_names import DATE, AMOUNT, CONVENTION_ID, DETAIL
from src.constants.csv import CONVENTION_CSV

CONVENTION_OBJECT = 'conv_objet'
CONV_OBJECT_OTHER = 'conv_objet_autre'
OTHER = 'Autre'


def convention_object(df):
    return df[CONVENTION_OBJECT].where(df[CONVENTION_OBJECT] != OTHER, df[CONV_OBJECT_OTHER])


def replace_zero_amount_by_none(df):
    return df[AMOUNT].replace(to_replace='0', value=pd.np.NaN)


cleaner = DeclarationCleaner(
    csv_name=CONVENTION_CSV,
    rename_columns={
        'conv_date_signature': DATE,
        'conv_montant_ttc': AMOUNT,
        'ligne_identifiant': CONVENTION_ID,
        'conv_objet': CONVENTION_OBJECT,
        'conv_objet_autre': CONV_OBJECT_OTHER
    },
    assign={
        DETAIL: convention_object,
        AMOUNT: replace_zero_amount_by_none,
    },
    drop_columns=[CONVENTION_OBJECT, CONV_OBJECT_OTHER]
)