from src.clean.cleaner import Cleaner
from src.constants.column_names import ISSUING_COMPANY_NAME, COUNTRY, SECTOR, COMPANY_ID, ADDRESS_LINE_1, \
    ADDRESS_LINE_2, ADDRESS_LINE_3, ADDRESS_LINE_4, POSTAL_CODE, CITY, ADDRESS, COUNTRY_CODE
from src.constants.csv import COMPANY_CSV, COMMA_SEPARATOR

ADDRESS_LINES = [ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_LINE_3, ADDRESS_LINE_4]


def address(df):
    s_address = df[ADDRESS_LINE_1].fillna('').str.rstrip()
    for line in ADDRESS_LINES[1:]:
        s_address += (', ' + df[line]).fillna('').str.rstrip()
    return s_address


cleaner = Cleaner(
    csv_name=COMPANY_CSV,
    raw_separator=COMMA_SEPARATOR,
    rename_columns={
        "identifiant": COMPANY_ID,
        "pays_code": COUNTRY_CODE,
        "pays": COUNTRY,
        # "secteur_activite_code": "secteur_activite_code",
        "secteur": SECTOR,
        "denomination_sociale": ISSUING_COMPANY_NAME,
        "adresse_1": ADDRESS_LINE_1,
        "adresse_2": ADDRESS_LINE_2,
        "adresse_3": ADDRESS_LINE_3,
        "adresse_4": ADDRESS_LINE_4,
        "code_postal": POSTAL_CODE,
        "ville": CITY,
    },
    assign={
        ADDRESS: address,
        COUNTRY_CODE: lambda df: df[COUNTRY_CODE].str.lstrip('[').str.rstrip(']')
    },
    drop_columns=ADDRESS_LINES
)
