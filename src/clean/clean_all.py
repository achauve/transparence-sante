from typing import List

from src.clean import health_directory, company
from src.clean.cleaner import Cleaner
from src.clean.declaration import remuneration, avantage, convention


def all_cleaners() -> List[Cleaner]:
    return [
        health_directory.cleaner,
        remuneration.cleaner,
        avantage.cleaner,
        convention.cleaner,
        company.cleaner
    ]


def run():
    for cleaner in all_cleaners():
        cleaner.run()


if __name__ == '__main__':
    run()
