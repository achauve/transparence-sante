from typing import List

from src.constants.column_names import NAME, SURNAME, PHYSICAL_PERSON_ID, NAME_SURNAME, ISSUING_COMPANY_NAME, CONVENTION_ID, AMOUNT, COUNTRY, SECTOR, COMPANY_ID
from src.constants.tables import HEALTH_DIRECTORY, DECLARATION_AVANTAGE, DECLARATION_CONVENTION, \
    DECLARATION_REMUNERATION, DECLARATION, COMPANY_DIRECTORY
from src.postgres.ddl.abstract_ddl import DDL
from src.postgres.ddl.create_index import SearchIndexDDL
from src.postgres.ddl.create_materialized_view import AggregateViewDDL
from src.postgres.ddl.create_view import DeclarationViewDDL


def all_analytics_ddl() -> List[DDL]:
    company_directory_search_indices = [
        SearchIndexDDL(COMPANY_DIRECTORY, column)
        for column in [COUNTRY, SECTOR, COMPANY_ID]
    ]
    health_directory_search_indices = [
        SearchIndexDDL(HEALTH_DIRECTORY, column)
        for column in [NAME, SURNAME, PHYSICAL_PERSON_ID]
    ]
    declaration_search_indices = [
        SearchIndexDDL(table, column)
        for column in [NAME_SURNAME, ISSUING_COMPANY_NAME, CONVENTION_ID]
        for table in [DECLARATION_AVANTAGE, DECLARATION_CONVENTION, DECLARATION_REMUNERATION]
    ]
    declaration_value_by_column_aggregates = [
        (AMOUNT, NAME_SURNAME),
        (AMOUNT, ISSUING_COMPANY_NAME)
    ]
    declaration_aggregate_views = [
        AggregateViewDDL(DECLARATION, value, column)
        for value, column in declaration_value_by_column_aggregates
    ]

    analytics_ddl = [DeclarationViewDDL()]
    analytics_ddl += health_directory_search_indices
    analytics_ddl += declaration_search_indices
    analytics_ddl += declaration_aggregate_views
    analytics_ddl += company_directory_search_indices
    return analytics_ddl


def reset():
    for ddl in all_analytics_ddl():
        ddl.reset()


def run():
    for ddl in all_analytics_ddl():
        ddl.run()


if __name__ == '__main__':
    # reset()
    run()
