import os
from typing import Dict, List

import pandas as pd

from src.constants.csv import SEMICOLON_SEPARATOR
from src.constants.directories import cleaned_path
from src.postgres.ddl.create_table import TableDDL
from src.postgres.utils import create_postgres_engine
from src.utils import timeit, comma_separated_columns


class Uploader:
    SEPARATOR = SEMICOLON_SEPARATOR

    COLUMN_LINE_TEMPLATE = "    {quoted_column_name} {column_type}"
    VACUUM_ANALYSE_TEMPLATE = "VACUUM ANALYSE {};"

    def __init__(self, csv_name, table_name=None,
                 columns_with_non_default_type: Dict[str, str] = None):
        self.csv_path = cleaned_path(csv_name)
        self.columns: List = []
        if os.path.exists(self.csv_path):
            self.columns = list(pd.read_csv(self.csv_path, sep=self.SEPARATOR, nrows=1).columns)
        else:
            print("Cleaned csv file '{}' does not exists".format(self.csv_path))
        self.table_name = table_name or csv_name[:-4]
        self.columns_with_non_default_type = columns_with_non_default_type or dict()
        self.engine = create_postgres_engine()

    def reset(self):
        self.table_ddl.reset()

    def run(self):
        self._create_table()
        self._copy_csv_to_table()

    @timeit
    def _copy_csv_to_table(self):
        print("Load csv '{}' into table '{}'".format(self.csv_path, self.table_name))
        connection = self.engine.raw_connection()
        with connection.cursor() as cursor:
            cursor.execute("SET datestyle = 'ISO,DMY';")
            with open(self.csv_path, 'rb') as csv_file:
                csv_file.readline()
                cursor.copy_expert(
                    """COPY {table_name}({columns}) FROM STDIN
                    WITH (FORMAT CSV, DELIMITER '{delimiter}', ENCODING utf8)"""
                    .format(table_name=self.table_name,
                            columns=comma_separated_columns(self.columns),
                            delimiter=self.SEPARATOR),
                    csv_file
                )
        connection.commit()
        self.table_ddl.vacuum_analyse()

    def _create_table(self):
        self.table_ddl.run()

    @property
    def table_ddl(self):
        return TableDDL(self.table_name, self.columns, self.columns_with_non_default_type)
