from sqlalchemy import create_engine, text

from config.database import POSTGRES_USER, POSTGRES_HOST, POSTGRES_PORT
from config.secrets import POSTGRES_PASSWORD
from src.utils import timeit

POSTGRES_URL = "postgres://{user}:{password}@{host}:{port}".format(user=POSTGRES_USER,
                                                                   password=POSTGRES_PASSWORD,
                                                                   host=POSTGRES_HOST,
                                                                   port=POSTGRES_PORT)


def create_postgres_engine():
    # Without AUTOCOMMIT's isolation_level, "CREATE MATERIALIZED VIEW" query silently fails
    # when there is a first line "SET work_mem … ;"
    return create_engine(POSTGRES_URL, isolation_level='AUTOCOMMIT')


@timeit
def execute_text_query(engine, raw_query):
    query = text(raw_query)
    print("Execute query : {}".format(query))
    result = engine.execute(query)
    if result.returns_rows:
        for row in result:
            print(row)
