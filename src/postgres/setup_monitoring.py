from src.postgres.utils import create_postgres_engine, execute_text_query


def run():
    engine = create_postgres_engine()
    with open("sql/setup_monitoring.sql", 'r') as f:
        query = f.read()
    execute_text_query(engine, query)


if __name__ == '__main__':
    run()