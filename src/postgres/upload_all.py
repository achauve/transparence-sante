from typing import List

from src.constants.column_names import DATE, AMOUNT
from src.constants.csv import HEALTH_DIRECTORY_CSV, COMPANY_CSV, REMUNERATION_CSV, AVANTAGE_CSV, CONVENTION_CSV
from src.constants.tables import HEALTH_DIRECTORY
from src.postgres.uploader import Uploader

declaration_column_type_dict = {DATE: 'DATE', AMOUNT: 'INT'}


def all_uploaders() -> List[Uploader]:
    return [
        Uploader(HEALTH_DIRECTORY_CSV, HEALTH_DIRECTORY),
        Uploader(COMPANY_CSV),
        Uploader(REMUNERATION_CSV,
                 columns_with_non_default_type=declaration_column_type_dict),
        Uploader(AVANTAGE_CSV,
                 columns_with_non_default_type=declaration_column_type_dict),
        Uploader(CONVENTION_CSV,
                 columns_with_non_default_type=declaration_column_type_dict)
    ]


def reset():
    for uploader in all_uploaders():
        uploader.reset()


def run():
    for uploader in all_uploaders():
        uploader.run()


if __name__ == '__main__':
    reset()
    run()
