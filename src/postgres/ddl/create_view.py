from src.constants.csv import REMUNERATION_CSV, AVANTAGE_CSV, CONVENTION_CSV
from src.constants.postgres import VIEW
from src.constants.tables import DECLARATION_REMUNERATION, DECLARATION_CONVENTION, \
    DECLARATION_AVANTAGE, DECLARATION
from src.constants.column_names import ID, DECLARATION_TYPE
from src.postgres.ddl.abstract_ddl import DDL
from src.postgres.uploader import Uploader
from src.utils import comma_separated_columns


class DeclarationViewDDL(DDL):
    DDL_ENTITY = VIEW
    DDL_TEMPLATE = """
CREATE OR REPLACE VIEW {name} AS (
    SELECT {columns} FROM {declaration_remuneration}
    UNION ALL
    SELECT {columns}  FROM {declaration_convention}
    UNION ALL
    SELECT {columns} FROM {declaration_avantage}
);
"""

    def __init__(self):
        super(DeclarationViewDDL, self).__init__()
        self.name = DECLARATION

    @property
    def ddl(self):
        return self.DDL_TEMPLATE.format(
            name=self.name,
            columns=self.columns,
            declaration_remuneration=DECLARATION_REMUNERATION,
            declaration_convention=DECLARATION_CONVENTION,
            declaration_avantage=DECLARATION_AVANTAGE,
        )

    @property
    def columns(self):
        common_columns = set(Uploader(REMUNERATION_CSV).columns)
        common_columns &= set(Uploader(AVANTAGE_CSV).columns)
        common_columns &= set(Uploader(CONVENTION_CSV).columns)
        id_column = "{declaration_type} || '_' || {id} as {declaration}_{id}".format(
            declaration_type=DECLARATION_TYPE,
            id=ID,
            declaration=DECLARATION
        )
        return id_column + ', ' + comma_separated_columns(sorted(common_columns))


if __name__ == '__main__':
    DeclarationViewDDL().reset()
    print(DeclarationViewDDL().ddl)
    DeclarationViewDDL().run()
