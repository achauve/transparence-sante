import os
from abc import ABC, abstractmethod

from src.constants.directories import DDL_DIR
from src.constants.postgres import INDEX
from src.postgres.utils import create_postgres_engine, execute_text_query
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


class DDL(ABC):
    STR_TEMPLATE = "CREATE_{entity}_{name}"
    DDL_ENTITY = ""
    DROP_TEMPLATE = "DROP {entity} IF EXISTS {name} CASCADE;\n"

    def __init__(self):
        self.engine = create_postgres_engine()

    def reset(self):
        drop_ddl = self.DROP_TEMPLATE.format(entity=self.DDL_ENTITY, name=self.name)
        execute_text_query(self.engine, drop_ddl)

    def run(self):
        if self.DDL_ENTITY != INDEX:
            self._save_ddl()
        execute_text_query(self.engine, self.ddl)

    def _save_ddl(self):
        ddl_path = os.path.join(DDL_DIR, str(self) + '.sql')
        with open(ddl_path, 'wb') as ddl_file:
            ddl_file.write(self.ddl.encode("utf-8"))

    def __str__(self):
        return self.STR_TEMPLATE.format(entity=self.DDL_ENTITY, name=self.name)

    @property
    @abstractmethod
    def ddl(self):
        pass


class VacuumedDDL(DDL):
    VACUUM_ANALYSE_TEMPLATE = """VACUUM ANALYSE "{name}";"""

    def __init__(self):
        super(VacuumedDDL, self).__init__()

    def run(self):
        super().run()
        self.vacuum_analyse()

    def vacuum_analyse(self):
        connection = self.engine.raw_connection()
        connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        vacuum_analyse_query = self.VACUUM_ANALYSE_TEMPLATE.format(name=self.name)
        with connection.cursor() as cursor:
            print("Execute query : {}".format(vacuum_analyse_query))
            cursor.execute(vacuum_analyse_query)
