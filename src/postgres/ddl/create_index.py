from src.constants.postgres import INDEX
from src.postgres.ddl.abstract_ddl import DDL


class IndexDDL(DDL):
    DDL_ENTITY = INDEX
    NAME_TEMPLATE = "{table}_index_on_{column}"
    DDL_TEMPLATE = """
CREATE INDEX IF NOT EXISTS "{name}"
ON public."{table}" ("{column}");
"""

    def __init__(self, table, column):
        super(IndexDDL, self).__init__()
        self.table = table
        self.column = column
        self.name = self.NAME_TEMPLATE.format(**self.__dict__)

    @property
    def ddl(self):
        return self.DDL_TEMPLATE.format(**self.__dict__)


class SearchIndexDDL(DDL):
    DDL_ENTITY = INDEX
    NAME_TEMPLATE = "{table}_prefix_index_on_{column}"
    DDL_TEMPLATE = """
CREATE INDEX IF NOT EXISTS "{name}"
ON public."{table}" (lower("{column}") text_pattern_ops, "{column}");
"""

    def __init__(self, table, column):
        super(SearchIndexDDL, self).__init__()
        self.table = table
        self.column = column
        self.name = self.NAME_TEMPLATE.format(**self.__dict__)

    @property
    def ddl(self):
        ddl = self.DDL_TEMPLATE.format(**self.__dict__)
        ddl += IndexDDL(self.table, self.column).ddl
        return ddl

