from src.constants.postgres import MATERIALIZED_VIEW
from src.postgres.ddl.abstract_ddl import VacuumedDDL
from src.postgres.ddl.create_index import SearchIndexDDL, IndexDDL


class AggregateViewDDL(VacuumedDDL):
    DDL_ENTITY = MATERIALIZED_VIEW
    NAME_TEMPLATE = "{table}_aggregate_{value}_by_{column}"
    AGGREGATE_COLUMN_TEMPLATE = "total_{value}"

    DDL_TEMPLATE = """
SET work_mem = '150MB'; -- allow HashAggregate rather than a slow GroupAggregate
CREATE MATERIALIZED VIEW IF NOT EXISTS "{name}" AS (
    SELECT
        "{column}",
        sum("{value}") AS "{aggregate_column}"
    FROM "{table}"
    WHERE (
        ("{column}" IS NOT NULL)
        AND ("{value}" IS NOT NULL)
    )
    GROUP BY "{column}"
    ORDER BY (sum("{value}")) DESC
);
RESET work_mem;
"""

    def __init__(self, table, value, column):
        super(AggregateViewDDL, self).__init__()
        self.table = table
        self.value = value
        self.column = column
        self.name = self.NAME_TEMPLATE.format(**self.__dict__)
        self.aggregate_column = self.AGGREGATE_COLUMN_TEMPLATE.format(**self.__dict__)

    @property
    def ddl(self):
        ddl = self.DDL_TEMPLATE.format(**self.__dict__)
        ddl += SearchIndexDDL(self.name, self.column).ddl
        ddl += IndexDDL(self.name, self.aggregate_column).ddl
        return ddl
