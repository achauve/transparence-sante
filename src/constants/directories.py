import os

WORKING_DIR_ABSOLUTE_PATH = os.getcwd()

DATA = 'data'
RAW_DATA_DIR = os.path.join(DATA, 'raw')
CLEANED_DATA_DIR = os.path.join(DATA, 'cleaned')
DDL_DIR = os.path.join('sql', 'ddl')

DOCKER_ABSOLUTE_PATH = os.path.join(WORKING_DIR_ABSOLUTE_PATH, "docker")
DOCKER_METABASE_DIR = os.path.join(DOCKER_ABSOLUTE_PATH, "metabase")
DOCKER_POSTGRES_DIR = os.path.join(DOCKER_ABSOLUTE_PATH, "postgres")
DOCKER_NGINX_DIR = os.path.join(DOCKER_ABSOLUTE_PATH, "nginx")

WEBSITE_DIR = os.path.join(WORKING_DIR_ABSOLUTE_PATH, "spike/006-jekyll-minimal-mistakes/site/_site")


def raw_path(file_name):
    return os.path.join(RAW_DATA_DIR, file_name)


def cleaned_path(file_name):
    return os.path.join(CLEANED_DATA_DIR, file_name)