ID = 'id'

# Health directory + Declaration
PHYSICAL_PERSON_ID = 'identifiant'
NAME = 'nom'
SURNAME = 'prénom'
NAME_SURNAME = NAME + '_' + SURNAME
PROFESSION = "profession"
SPECIALTY = "specialité"

# Declaration
CATEGORY = 'catégorie'
ISSUING_COMPANY_NAME = 'entreprise_émmetrice'
DECLARATION_TYPE = 'type_declaration'
ID_TYPE = 'type_identifiant'
ADVANTAGE = 'Avantage'
CONVENTION = 'Convention'
REMUNERATION = 'Remunération'
DATE = 'date'
AMOUNT = 'montant_ttc'
CONVENTION_ID = 'identifiant_convention'
DETAIL = 'detail'

IS_AMOUNT_MASKED = 'montant_masque'

# Company directory
COUNTRY = "pays"
SECTOR = "secteur"
COMPANY_ID = "identifiant_entreprise"
ADDRESS = "adresse"
ADDRESS_LINE_1 = "adresse_1"
ADDRESS_LINE_2 = "adresse_2"
ADDRESS_LINE_3 = "adresse_3"
ADDRESS_LINE_4 = "adresse_4"
POSTAL_CODE = "code_postal"
CITY = "ville"
COUNTRY_CODE = "code_pays"
