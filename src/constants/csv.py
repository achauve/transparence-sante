SEMICOLON_SEPARATOR = ';'
COMMA_SEPARATOR = ','

COMPANY_CSV = 'entreprise.csv'
REMUNERATION_CSV = 'declaration_remuneration.csv'
AVANTAGE_CSV = 'declaration_avantage.csv'
CONVENTION_CSV = 'declaration_convention.csv'

HEALTH_DIRECTORY_CSV = "ExtractionMonoTable_CAT18_ToutePopulation.csv"

DECLARATION_CSV_LIST = [REMUNERATION_CSV,
                        AVANTAGE_CSV,
                        CONVENTION_CSV]

ETALAB_CSV_LIST = [COMPANY_CSV] + DECLARATION_CSV_LIST
CSV_LIST = [HEALTH_DIRECTORY_CSV] + ETALAB_CSV_LIST
