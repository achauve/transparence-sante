import os
import zipfile

import requests

from config.read_csv import CHUNK_SIZE
from src.constants.csv import ETALAB_CSV_LIST, HEALTH_DIRECTORY_CSV
from src.constants.directories import RAW_DATA_DIR
from src.utils import prefix

# https://www.data.gouv.fr/fr/users/pierre-alain-jachiet/
ETALAB_URL = 'https://www.transparence.sante.gouv.fr/exports-etalab/exports-etalab.zip'
ETALAB_ZIP_NAME = 'exports-etalab.zip'

# http://esante.gouv.fr/sites/default/files/asset/document/annuaire_sante_fr_dsft_fichier_extraction_rpps_donnees_accessibles_v1.0.0.pdf
DIRECTORY_URL = "https://service.annuaire.sante.fr" \
               "/annuaire-sante-webservices/V300/services/extraction/ExtractionMonoTable_CAT18_ToutePopulation"
DIRECTORY_ZIP_NAME = 'annuaire_sante.zip'


def download_raw_data():
    download_and_extract_csv(ETALAB_URL, ETALAB_ZIP_NAME, ETALAB_CSV_LIST)
    download_and_extract_csv(DIRECTORY_URL, DIRECTORY_ZIP_NAME,
                             [HEALTH_DIRECTORY_CSV], verify=False)


def download_and_extract_csv(url, zip_name, csv_list, verify=True):
    zip_path = os.path.join(RAW_DATA_DIR, zip_name)
    download_file_from_url(url, zip_path, verify=verify)
    extract_csv_list_from_zip(RAW_DATA_DIR, zip_name, csv_list)
    # os.remove(zip_path)


def download_file_from_url(url, file_path, verify=True):
    print("Download file at url {} to '{}'".format(url, file_path))
    r = requests.get(url, stream=True, verify=verify)
    with open(file_path, 'wb') as f:
        for chunk in r.iter_content(CHUNK_SIZE):
            f.write(chunk)


def extract_csv_list_from_zip(dir_path, zip_name, csv_list):
    zip_path = os.path.join(dir_path, zip_name)
    with zipfile.ZipFile(zip_path) as zf:
        for zip_file_name in zf.namelist():
            for csv_name in csv_list:
                csv_prefix = prefix(csv_name)
                if zip_file_name.startswith(csv_prefix):
                    raw_csv_path = os.path.join(dir_path, csv_name)
                    print("Extract '{}' as '{}'".format(zip_file_name, csv_name))
                    with zf.open(zip_file_name) as input_file, open(raw_csv_path, 'wb') as output_file:
                        for line in input_file:
                            output_file.write(line)


if __name__ == "__main__":
    # download_raw_data()

    download_file_from_url(ETALAB_URL, os.path.join(RAW_DATA_DIR, ETALAB_ZIP_NAME), verify=True)
